Démineur
==

## Description

Le démineur est un jeu qui consiste à découvrir toutes les cases libres de la grille sans faire exploser de mine.
Lorsque le joueur clique sur une case libre comportant au moins une mine dans l'une de ses cases avoisinantes, un chiffre apparaît, indiquant ce nombre de mines. Si en revanche toutes les cases adjacentes sont vides, une case vide est affichée et la même opération est répétée sur ces cases, et ce jusqu'à ce que la zone vide soit entièrement délimitée par des chiffres. En comparant les différentes informations récoltées, le joueur peut ainsi progresser dans le déminage du terrain. S'il se trompe et clique sur une mine, il a perdu. 


## Équipe de développement

LP RGI - WEB
--
| Nom      |  Prénom |
| --------:| -------:|
| LECLERCQ | Yanis   |
| DAVENNE  | Nicolas |
| SCHAMEL  | Yacine  |
| MOKHTARI | Samy    |

 
## Framework et technologies utilisés

 + C#
 + WPF (XAML)
  
![C#](https://2.bp.blogspot.com/-Tm6i7Ays-LM/XIZyynW_jsI/AAAAAAAAQDg/XjzkRbvYK9krPoPwxmwW9tLJoaC4eTRcgCK4BGAYYCw/s400/2019-03-11_15h37_34.png)
![WPF](https://www.ambient-it.net/wp-content/uploads/2016/04/wpf-logo-175.png)

Visit https://docs.microsoft.com/fr-fr/