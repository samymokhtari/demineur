﻿using System;

namespace Demineur
{
    class Program
    {
        const int NB_MINES_FACILE = 10;
        const int NB_MINES_MOYEN = 40;
        const int NB_MINES_DIFFICILE = 99;

        const int FORMAT_FACILE = 9;
        const int FORMAT_MOYEN = 16;
        const int FORMAT_DIFFICILE = 30;

        // 1 - On choisit le niveau du Démineur parmi 3 choix 
        public static int choisirNiveau()
        {
            int niveauChoisi;

            do
            {
                Console.WriteLine("Choisissez le niveau de difficulté : \n (1) - Facile (9x9) \n (2) - Moyen (16x16) \n (3) - Difficile (30x16) \n");

                string niveauChoisiStr = Console.ReadLine(); int.TryParse(niveauChoisiStr, out niveauChoisi); Console.Clear();

                Console.WriteLine("Niveau choisi : " + niveauChoisi + "\n");
                if (niveauChoisi < 1 || niveauChoisi > 3) Console.WriteLine("Veuillez faire un choix valide !");
            } while (niveauChoisi < 1 || niveauChoisi > 3);

            return niveauChoisi;
        }

        // 2 - On génère un plateau de jeu essentiellement de 0 (cellules non jouées) correspondant au niveau choisi
        public static int[,] initierTableauDeJeu(int niveauChoisi)
        {
            switch (niveauChoisi)
            {
                case 1:
                    return new int[FORMAT_FACILE, FORMAT_FACILE];
                case 2:
                    return new int[FORMAT_MOYEN, FORMAT_MOYEN];
                case 3:
                    return new int[FORMAT_DIFFICILE, FORMAT_MOYEN];
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        // 3 - Ce n'est qu'après avoir joué le premier coup que l'on peut générer de façon aléatoire la position des mines. On s'assure que le premier coup a lieu avant !!!
        // 3.1 - Revoir les paramètres de cette fonction car à aucun moment plateauDemineur n'a connaissance du plateau initial
        public static int[,] initialiserLesMines(int niveauChoisi, int lignePremierCoup, int colonnePremierCoup)
        {

            int nbMinesRestantes;
            int nbLignes;
            int nbColonnes;
            int[,] plateauDemineur;

            switch (niveauChoisi)
            {
                case 1:
                    nbMinesRestantes = NB_MINES_FACILE;
                    nbLignes = FORMAT_FACILE;
                    nbColonnes = FORMAT_FACILE;
                    plateauDemineur = new int[nbLignes, nbColonnes];
                    break;
                case 2:
                    nbMinesRestantes = NB_MINES_MOYEN;
                    nbLignes = FORMAT_MOYEN;
                    nbColonnes = FORMAT_MOYEN;
                    plateauDemineur = new int[nbLignes, nbColonnes];
                    break;
                case 3:
                    nbMinesRestantes = NB_MINES_DIFFICILE;
                    nbLignes = FORMAT_DIFFICILE;
                    nbColonnes = FORMAT_MOYEN;
                    plateauDemineur = new int[nbLignes, nbColonnes];
                    break;
                default:
                    nbMinesRestantes = 0;
                    nbLignes = 0;
                    nbColonnes = 0;
                    plateauDemineur = new int[nbLignes, nbColonnes];
                    break;
            }

            Random nbAleatoire = new Random();

            do
            {
                for (int cptLigne = 0; cptLigne < nbLignes; cptLigne++)
                    for (int cptCol = 0; cptCol < nbColonnes; cptCol++)
                    {
                        if (cptLigne == lignePremierCoup && cptCol == colonnePremierCoup) { }
                        else if (plateauDemineur[cptLigne, cptCol] == 0 && nbMinesRestantes > 0) // La condition == 0 est suffisante pour s'assurer que ce ne soit pas une case jouée
                        {
                            int nbGenere = nbAleatoire.Next(0, 10);
                            if (nbGenere == 3)
                            {
                                plateauDemineur[cptLigne, cptCol] = 1;
                                //Console.WriteLine("Je mets une mine : " + nbMinesRestantes);
                                nbMinesRestantes--;
                            }

                        }
                    }

            } while (nbMinesRestantes > 0);

            return plateauDemineur;
        }

        // 4 - Les mines placées, il nous est maintenant possible de compter le nombre de mines voisines à chaque cellule jouable !
        public static int[,] compterLesMinesVoisines(int niveauChoisi, int[,] tableauDeMines)
        {
            int nbLignes;
            int nbColonnes;
            int[,] plateauDemineur = tableauDeMines;

            switch (niveauChoisi)
            {
                case 1:
                    nbLignes = FORMAT_FACILE;
                    nbColonnes = FORMAT_FACILE;
                    break;
                case 2:
                    nbLignes = FORMAT_MOYEN;
                    nbColonnes = FORMAT_MOYEN;
                    break;
                case 3:
                    nbLignes = FORMAT_DIFFICILE;
                    nbColonnes = FORMAT_MOYEN;
                    break;
                default:
                    nbLignes = 0;
                    nbColonnes = 0;
                    break;
            }

            int[,] tableauDeVoisins = new int[nbLignes, nbColonnes];

            for (int cptLigne = 0; cptLigne < nbLignes; cptLigne++)
                for (int cptCol = 0; cptCol < nbColonnes; cptCol++)
                {
                    if (plateauDemineur[cptLigne, cptCol] != 1) // On s'assure de ne compter le nombre de voisins uniquement pour les cellules jouables (!=1)
                    {
                        int nbMinesVoisines = 0;

                        // Ligne supérieure
                        if (cptLigne - 1 >= 0 && cptCol - 1 >= 0)
                            if (plateauDemineur[cptLigne - 1, cptCol - 1] == 1)
                                nbMinesVoisines++;

                        if (cptLigne - 1 >= 0)
                            if (plateauDemineur[cptLigne - 1, cptCol] == 1)
                                nbMinesVoisines++;

                        if (cptLigne - 1 >= 0 && cptCol + 1 < nbColonnes)
                            if (plateauDemineur[cptLigne - 1, cptCol + 1] == 1)
                                nbMinesVoisines++;

                        // Ligne courante
                        if (cptCol - 1 >= 0)
                            if (plateauDemineur[cptLigne, cptCol - 1] == 1)
                                nbMinesVoisines++;

                        if (cptCol + 1 < nbColonnes)
                            if (plateauDemineur[cptLigne, cptCol + 1] == 1)
                                nbMinesVoisines++;

                        // Ligne inférieure
                        if (cptLigne + 1 < nbLignes && cptCol - 1 >= 0)
                            if (plateauDemineur[cptLigne + 1, cptCol - 1] == 1)
                                nbMinesVoisines++;

                        if (cptLigne + 1 < nbLignes)
                            if (plateauDemineur[cptLigne + 1, cptCol] == 1)
                                nbMinesVoisines++;

                        if (cptLigne + 1 < nbLignes && cptCol + 1 < nbColonnes)
                            if (plateauDemineur[cptLigne + 1, cptCol + 1] == 1)
                                nbMinesVoisines++;

                        tableauDeVoisins[cptLigne, cptCol] = nbMinesVoisines;
                    }
                }

            return tableauDeVoisins;
        }

        // 5 - On peut maintenant afficher un plateau de jeu en fonction du niveau
        public static void afficherUnPlateau(int niveauChoisi, int[,] plateau)
        {
            int nbLignes;
            int nbColonnes;

            switch (niveauChoisi)
            {
                case 1:
                    nbLignes = FORMAT_FACILE;
                    nbColonnes = FORMAT_FACILE;
                    break;
                case 2:
                    nbLignes = FORMAT_MOYEN;
                    nbColonnes = FORMAT_MOYEN;
                    break;
                case 3:
                    nbLignes = FORMAT_DIFFICILE;
                    nbColonnes = FORMAT_MOYEN;
                    break;
                default:
                    nbLignes = 0;
                    nbColonnes = 0;
                    break;
            }

            for (int cptLigne = 0; cptLigne < nbLignes; cptLigne++)
                for (int cptCol = 0; cptCol < nbColonnes; cptCol++)
                {
                    if (cptCol == nbColonnes - 1) { Console.WriteLine(plateau[cptLigne, cptCol]); }
                    else { Console.Write(plateau[cptLigne, cptCol]); }
                }
        }

        public static void afficherUnPlateauString(int niveauChoisi, string[,] plateau)
        {
            int nbLignes;
            int nbColonnes;

            switch (niveauChoisi)
            {
                case 1:
                    nbLignes = FORMAT_FACILE;
                    nbColonnes = FORMAT_FACILE;
                    break;
                case 2:
                    nbLignes = FORMAT_MOYEN;
                    nbColonnes = FORMAT_MOYEN;
                    break;
                case 3:
                    nbLignes = FORMAT_DIFFICILE;
                    nbColonnes = FORMAT_MOYEN;
                    break;
                default:
                    nbLignes = 0;
                    nbColonnes = 0;
                    break;
            }

            for (int cptLigne = 0; cptLigne < nbLignes; cptLigne++)
                for (int cptCol = 0; cptCol < nbColonnes; cptCol++)
                {
                    if (cptCol == nbColonnes - 1) { Console.WriteLine(plateau[cptLigne, cptCol]); }
                    else { Console.Write(plateau[cptLigne, cptCol]); }
                }
        }

        // 6 - On s'assure qu'un coup joué est valide
        public static bool isCoupValide(int[,] tableauDeMines, int ligneJouee, int colonneJouee)
        {
            if (tableauDeMines[ligneJouee, colonneJouee] != 1)
                return true;

            return false;
        }

        // 7 - L'important au démineur c'est de pouvoir positionner les mines en fonction du nombre de mines adjacentes que l'on découvre après chaque coup.
        // La fonction suivante permet de découvrir ces nombres et d'initialiser l'affichage en conséquence
        public static void devoilerLesZerosVoisins(int niveauChoisi, int[,] tableauDuJoueur, int[,] tableauDeMines, int[,] tableauDesVoisins, int ligneJouee, int colonneJouee)
        {
            int nbLignes;
            int nbColonnes;

            // Ici on peut gérer le nombre de mines découvertes !

            switch (niveauChoisi)
            {
                case 1:
                    nbLignes = FORMAT_FACILE;
                    nbColonnes = FORMAT_FACILE;
                    break;
                case 2:
                    nbLignes = FORMAT_MOYEN;
                    nbColonnes = FORMAT_MOYEN;
                    break;
                case 3:
                    nbLignes = FORMAT_DIFFICILE;
                    nbColonnes = FORMAT_MOYEN;
                    break;
                default:
                    nbLignes = 0;
                    nbColonnes = 0;
                    break;
            }

            // On utilise la fonction par récurrence pour faire apparaître toutes les cellules voisines au coup courant, qui n'ont pas de mines adjacentes
            // On s'assure que la cellule voisine est également une cellule jouable
            if (tableauDesVoisins[ligneJouee, colonneJouee] == 0)
            {
                Console.WriteLine("Je fais face à une cellule à 0 mine voisine !");

                // Ligne supérieure 

                if (ligneJouee - 1 >= 0  && colonneJouee - 1 >= 0 && tableauDeMines[ligneJouee - 1, colonneJouee - 1] != 1 && tableauDuJoueur[ligneJouee - 1, colonneJouee - 1] != 9)
                {
                    tableauDuJoueur[ligneJouee - 1, colonneJouee - 1] = 9;
                    devoilerLesZerosVoisins(niveauChoisi, tableauDuJoueur, tableauDeMines, tableauDesVoisins, ligneJouee - 1, colonneJouee - 1);
                }

                if (ligneJouee - 1 >= 0 && tableauDeMines[ligneJouee - 1, colonneJouee] != 1 && tableauDuJoueur[ligneJouee - 1, colonneJouee] != 9)
                {
                    tableauDuJoueur[ligneJouee - 1, colonneJouee] = 9;
                    devoilerLesZerosVoisins(niveauChoisi, tableauDuJoueur, tableauDeMines, tableauDesVoisins, ligneJouee - 1, colonneJouee);
                }

                if (ligneJouee - 1 >= 0 && colonneJouee + 1 < nbColonnes && tableauDeMines[ligneJouee - 1, colonneJouee + 1] != 1 && tableauDuJoueur[ligneJouee - 1, colonneJouee + 1] != 9)
                {
                    tableauDuJoueur[ligneJouee - 1, colonneJouee + 1] = 9;
                    devoilerLesZerosVoisins(niveauChoisi, tableauDuJoueur, tableauDeMines, tableauDesVoisins, ligneJouee - 1, colonneJouee + 1);
                }

                // Ligne courante

                if (colonneJouee - 1 >= 0 && tableauDeMines[ligneJouee, colonneJouee - 1] != 1 && tableauDuJoueur[ligneJouee, colonneJouee - 1] != 9)
                {
                    tableauDuJoueur[ligneJouee, colonneJouee - 1] = 9;
                    devoilerLesZerosVoisins(niveauChoisi, tableauDuJoueur, tableauDeMines, tableauDesVoisins, ligneJouee, colonneJouee - 1);
                }

                if (colonneJouee + 1 < nbColonnes && tableauDeMines[ligneJouee, colonneJouee + 1] != 1 && tableauDuJoueur[ligneJouee, colonneJouee + 1] != 9)
                {
                    tableauDuJoueur[ligneJouee, colonneJouee + 1] = 9;
                    devoilerLesZerosVoisins(niveauChoisi, tableauDuJoueur, tableauDeMines, tableauDesVoisins, ligneJouee, colonneJouee + 1);
                }

                // Ligne inférieure

                if (ligneJouee + 1 < nbLignes && colonneJouee - 1 >= 0 && tableauDeMines[ligneJouee + 1, colonneJouee - 1] != 1 && tableauDuJoueur[ligneJouee + 1, colonneJouee - 1] != 9)
                {
                    tableauDuJoueur[ligneJouee + 1, colonneJouee - 1] = 9;
                    devoilerLesZerosVoisins(niveauChoisi, tableauDuJoueur, tableauDeMines, tableauDesVoisins, ligneJouee + 1, colonneJouee - 1);
                }

                if (ligneJouee + 1 < nbLignes && tableauDeMines[ligneJouee + 1, colonneJouee] != 1 && tableauDuJoueur[ligneJouee + 1, colonneJouee] != 9)
                {
                    tableauDuJoueur[ligneJouee + 1, colonneJouee] = 9;
                    devoilerLesZerosVoisins(niveauChoisi, tableauDuJoueur, tableauDeMines, tableauDesVoisins, ligneJouee + 1, colonneJouee);
                }

                if (ligneJouee + 1 < nbLignes && colonneJouee + 1 < nbColonnes && tableauDeMines[ligneJouee + 1, colonneJouee + 1] != 1 && tableauDuJoueur[ligneJouee + 1, colonneJouee + 1] != 9)
                {
                    tableauDuJoueur[ligneJouee + 1, colonneJouee + 1] = 9;
                    devoilerLesZerosVoisins(niveauChoisi, tableauDuJoueur, tableauDeMines, tableauDesVoisins, ligneJouee + 1, colonneJouee + 1);
                }

            }
            else
            {
                tableauDuJoueur[ligneJouee, colonneJouee] = tableauDesVoisins[ligneJouee, colonneJouee];
            }
        }

        // 8 - Voir le nombre de voisins
        public static string[,] devoilerLeNombreDeMinesVoisines(int niveauChoisi, int[,] plateauJoueur, int[,] plateauDeMinesVoisines)
        {
            int nbLignes, nbColonnes, nbMines, nbCellulesSansMines;
            string[,] tableauJoueur ;

            switch (niveauChoisi)
            {
                case 1:
                    nbLignes = FORMAT_FACILE;
                    nbColonnes = FORMAT_FACILE;
                    nbMines = NB_MINES_FACILE;
                    nbCellulesSansMines = nbLignes * nbColonnes - nbMines;
                    tableauJoueur = new string[nbLignes, nbColonnes];
                    break;
                case 2:
                    nbLignes = FORMAT_MOYEN;
                    nbColonnes = FORMAT_MOYEN;
                    nbMines = NB_MINES_MOYEN;
                    nbCellulesSansMines = nbLignes * nbColonnes - nbMines;
                    tableauJoueur = new string[nbLignes, nbColonnes];
                    break;
                case 3:
                    nbLignes = FORMAT_DIFFICILE;
                    nbColonnes = FORMAT_MOYEN;
                    nbMines = NB_MINES_DIFFICILE;
                    nbCellulesSansMines = nbLignes * nbColonnes - nbMines;
                    tableauJoueur = new string[nbLignes, nbColonnes];
                    break;
                default:
                    nbLignes = 0;
                    nbColonnes = 0;
                    nbMines = 0;
                    nbCellulesSansMines = nbLignes * nbColonnes - nbMines;
                    tableauJoueur = new string[nbLignes, nbColonnes];
                    break;
            }

            for (int cptLigne = 0; cptLigne < nbLignes; cptLigne++)
                for (int cptCol = 0; cptCol < nbColonnes; cptCol++)
                {
                    string valueToDisplay;

                    if (plateauJoueur[cptLigne, cptCol] == 9) { if (plateauDeMinesVoisines[cptLigne, cptCol] == 0) { valueToDisplay = " ";} else { valueToDisplay = plateauDeMinesVoisines[cptLigne, cptCol].ToString(); } }
                    else if (plateauJoueur[cptLigne, cptCol] == 10) { valueToDisplay = "M"; }
                    else { valueToDisplay = Convert.ToString(plateauJoueur[cptLigne, cptCol]); }

                    if (cptCol == nbColonnes - 1) { Console.WriteLine(valueToDisplay); }
                    else { Console.Write(valueToDisplay); }

                    tableauJoueur[cptLigne, cptCol] = valueToDisplay;
                }

            return tableauJoueur;
        }

        // 9 - Jouer un coup en choisissant les coordonnées X et Y de la cellule sélectionnée
        public static void jouerUnCoup(int niveauChoisi, int[,] tableauDuJoueur, int[,] tableauDeMines, int[,] tableauDesVoisins, int nbCellulesDecouvertes)
        {
            int ligneJouee, colonneJouee;
            string[,] tableauJoueur;

            /* Le joueur a la possibilité de jouer différents types de coups : (On implémentera ces fonctionnalités plus tard)
                * J - Jouer une cellule (Gérer les cellules voisines avec les zéros et incrémenter le nombre de cellules découvertes)
                * Dans ce cas, la cellule jouée sera marquée 9
                * M - Marquer une mine (Avoir un compteur de mines marquées)
                * Si le joueur fait le choix de marquer la cellule, cette dernière sera marqué 10
                * La difficulté résidera dans l'affichage adéquat des tableaux
                */

            Console.WriteLine("---Tableau Joueur---");
            tableauJoueur = devoilerLeNombreDeMinesVoisines(niveauChoisi, tableauDuJoueur, tableauDesVoisins);
            Console.WriteLine("---Tableau Joueur V2---");
            afficherUnPlateauString(niveauChoisi, tableauJoueur);
            Console.WriteLine("---Tableau De Position De Mines---");
            afficherUnPlateau(niveauChoisi, tableauDeMines);
            Console.WriteLine("---Tableau Des Voisins---");
            afficherUnPlateau(niveauChoisi, tableauDesVoisins);

            Console.WriteLine("Quel coup souhaitez-vous jouer ? \nJ - Jouer une cellule \nM - Marquer une mine");
            char choixDuJoueur = Console.ReadKey().KeyChar;
            Console.WriteLine("\n");

            Console.Write("Quelle Ligne ? ");
            int.TryParse(Console.ReadLine(), out ligneJouee);
            Console.Write("Quelle Colonne ? ");
            int.TryParse(Console.ReadLine(), out colonneJouee);

            if (choixDuJoueur == 'J')
            {
                if (isCoupValide(tableauDeMines, ligneJouee, colonneJouee))
                {
                    nbCellulesDecouvertes++;
                    tableauDuJoueur[ligneJouee, colonneJouee] = 9;
                    devoilerLesZerosVoisins(niveauChoisi, tableauDuJoueur, tableauDeMines, tableauDesVoisins, ligneJouee, colonneJouee);
                    //Console.WriteLine("Vous avez découvert {0} cellules !", nbCellulesDecouvertes);
                }
                else
                {
                    throw new Exception();
                }
            }
            else if (choixDuJoueur == 'M' && tableauDuJoueur[ligneJouee, colonneJouee] != 9) {
                tableauDuJoueur[ligneJouee, colonneJouee] = 10; 
                devoilerLesZerosVoisins(niveauChoisi, tableauDuJoueur, tableauDeMines, tableauDesVoisins, ligneJouee, colonneJouee);
            }
        }


        static void Main(string[] args)
        {

            int niveauChoisi = choisirNiveau();

            int nbLignes, nbColonnes;
            int nbMines, nbCellulesSansMines;
            int nbCellulesDecouvertes = 0;

            bool isFirst = true;

            int[,] plateauJoueur;
            int[,] plateauDePositionsMines;
            int[,] plateauDesNombresDeVoisins;

            switch (niveauChoisi)
            {
                case 1:
                    nbLignes = FORMAT_FACILE;
                    nbColonnes = FORMAT_FACILE;
                    nbMines = NB_MINES_FACILE;
                    nbCellulesSansMines = nbLignes * nbColonnes - nbMines;
                    plateauJoueur = new int[nbLignes, nbColonnes];
                    plateauDePositionsMines = new int[nbLignes, nbColonnes];
                    plateauDesNombresDeVoisins = new int[nbLignes, nbColonnes];
                    break;
                case 2:
                    nbLignes = FORMAT_MOYEN;
                    nbColonnes = FORMAT_MOYEN;
                    nbMines = NB_MINES_MOYEN;
                    nbCellulesSansMines = nbLignes * nbColonnes - nbMines;
                    plateauJoueur = new int[nbLignes, nbColonnes];
                    plateauDePositionsMines = new int[nbLignes, nbColonnes];
                    plateauDesNombresDeVoisins = new int[nbLignes, nbColonnes];
                    break;
                case 3:
                    nbLignes = FORMAT_DIFFICILE;
                    nbColonnes = FORMAT_MOYEN;
                    nbMines = NB_MINES_DIFFICILE;
                    nbCellulesSansMines = nbLignes * nbColonnes - nbMines;
                    plateauJoueur = new int[nbLignes, nbColonnes];
                    plateauDePositionsMines = new int[nbLignes, nbColonnes];
                    plateauDesNombresDeVoisins = new int[nbLignes, nbColonnes];
                    break;
                default:
                    nbLignes = 0;
                    nbColonnes = 0;
                    nbMines = 0;
                    nbCellulesSansMines = nbLignes * nbColonnes - nbMines;
                    plateauJoueur = new int[nbLignes, nbColonnes];
                    plateauDePositionsMines = new int[nbLignes, nbColonnes];
                    plateauDesNombresDeVoisins = new int[nbLignes, nbColonnes];
                    break;
            }

            do
            {
                if (isFirst)
                {
                    int ligneJouee, colonneJouee;
                    Console.WriteLine("Jouez votre premier coup !");

                    Console.Write("Quelle Ligne ? ");
                    int.TryParse(Console.ReadLine(), out ligneJouee);
                    Console.Write("Quelle Colonne ? ");
                    int.TryParse(Console.ReadLine(), out colonneJouee);

                    plateauJoueur[ligneJouee, colonneJouee] = 9;

                    plateauDePositionsMines = initialiserLesMines(niveauChoisi, ligneJouee, colonneJouee);
                    plateauDesNombresDeVoisins = compterLesMinesVoisines(niveauChoisi, plateauDePositionsMines);
                    isFirst = false;

                    Console.WriteLine("---Tableau Joueur---");
                    devoilerLesZerosVoisins(niveauChoisi, plateauJoueur, plateauDePositionsMines, plateauDesNombresDeVoisins, ligneJouee, colonneJouee);
                    Console.WriteLine("---Tableau De Position De Mines---");
                    afficherUnPlateau(niveauChoisi, plateauDePositionsMines);
                    Console.WriteLine("---Tableau Des Voisins---");
                    afficherUnPlateau(niveauChoisi, plateauDesNombresDeVoisins);

                    Console.Clear();
                } else
                {
                    try
                    {
                        jouerUnCoup(niveauChoisi, plateauJoueur, plateauDePositionsMines, plateauDesNombresDeVoisins, nbCellulesDecouvertes);
                        Console.Clear();
                    }
                    catch (Exception e)
                    {
                        Console.Clear();  Console.WriteLine(e);  Console.WriteLine("                          oooo$$$$$$$$$$$$oooo \n" +
                            "                      oo$$$$$$$$$$$$$$$$$$$$$$$$o \n" +
                            "                   oo$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$o         o$   $$ o$ \n" +
                            "   o $ oo        o$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$o       $$ $$ $$o$ \n" +
                            "oo $ $  $      o$$$$$$$$$    $$$$$$$$$$$$$    $$$$$$$$$o       $$$o$$o$\n" +
                            " $$$$$$o$     o$$$$$$$$$      $$$$$$$$$$$      $$$$$$$$$$o    $$$$$$$$\n" +
                            "  $$$$$$$    $$$$$$$$$$$      $$$$$$$$$$$      $$$$$$$$$$$$$$$$$$$$$$$\n" +
                            "  $$$$$$$$$$$$$$$$$$$$$$$    $$$$$$$$$$$$$    $$$$$$$$$$$$$$     $$$\n" +
                            "    $$$    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$      $$$\n" +
                            "    $$$   o$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$      $$$o\n" +
                            "   o$$    $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$       $$$o\n" +
                            "   $$$    $$$$$$$$$$$$$$$$$$$GAME$OVER$$$$$$$$$$$$$$$$$   $$$$$$ooooo$$$$o\n" +
                            "  o$$$oooo$$$$$  $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$   o$$$$$$$$$$$$$$$$$\n" +
                            "  $$$$$$$$ $$$$   $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$     $$$$\n" +
                            "            $$$$     $$$$$$$$$$$$$$$$$$$$$$$$$$$$       o$$$\n" +
                            "             $$$o        $$$$$$$$$$$$$$$$$$ $$          $$$\n" +
                            "              $$$o           $$  $$$$$$               o$$$\n" +
                            "                $$$$o                                o$$$ \n" +
                            "                  $$$$o       o$$$$$$o $$$$o        o$$$$\n" +
                            "                    $$$$$oo       $$$$o$$$$$o   o$$$$ \n" +
                            "                        $$$$$oooo   $$$o$$$$$$$$$   \n" +
                            "                           $$$$$$$oo $$$$$$$$$$\n" +
                            "                                     $$$$$$$$$$$\n" +
                            "                                     $$$$$$$$$$$$\n" +
                            "                                      $$$$$$$$$$ \n" +
                            "                                           $$$ \n"); return;
                    }
                    
                }

            } while (nbCellulesDecouvertes < nbCellulesSansMines);

        }
    }
}